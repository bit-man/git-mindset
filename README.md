# Git Mindset

A new set of commands with a git mindset that can be used as git commands

## Install

### using bpkg

```
curl -sLo- https://get.bpkg.sh | bash
bpkg install bit-man/git-mindset -g
```

### clone
```
git clone https://gitlab.com/bit-man/git-mindset.git
export PATH="`pwd`/git-mindset/bin:$PATH"
```

## Usage

Just as any other command, `git <tab>` will expand to show available
commands

![git tab](img/git-tab.gif)
