
SCRIPTS = git-add-modified git-commit-emoji git-pull-all \
          git-search git-wash git-branch-delete \
		  git-pending git-restore-all 
LIBS = common.bash git.bash
PREFIX ?= /usr/local

LIST = one two three
install:
	for i in $(SCRIPTS); do \
		cp bin/$$i $(PREFIX)/bin/$$i; \
	done
	for i in $(LIBS); do \
		cp lib/$$i $(PREFIX)/lib/$$i; \
	done

uninstall:
	for i in $(SCRIPTS); do \
		rm $(PREFIX)/bin/$$i; \
	done
	for i in $(LIBS); do \
		rm $(PREFIX)/lib/$$i; \
	done
