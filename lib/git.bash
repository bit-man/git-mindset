#!/usr/bin/env bash

#
# Calls function passed in parameter $1 for each branch when pending changes exist
#
function __callOnPendingChanges() {
        let __pendings=`git status --porcelain | wc -l | tr -d " " `+`git status | grep --count ahead`
        [[ __pendings -ne 0 ]] && $1 $2 $3
}

#
# Calls function passed in parameter $1 for each branch
#
function __GitBranch() {
     for __branch in `git branch | tr '*' ' '`; do

        git checkout $__branch >/dev/null 2>&1
        $1 $__folder $__branch
     done
}

#
# Calls function passed in parameter $1 for each branch when pending changes exist
#
function __GitBranchApply() {
     __GitBranch "__callOnPendingChanges $1"
}
